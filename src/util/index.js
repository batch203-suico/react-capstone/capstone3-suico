import axios from 'axios';

const jacsAxios = axios.create({
  baseURL: 'https://safe-ridge-26228.herokuapp.com',
  // baseURL: 'http://localhost:4000',
  headers: {
    "Accept": 'application/json',
    "Content-Type": 'application/json'
  }
})

jacsAxios.interceptors.request.use((config) => {
  console.log(config)
  const savedToken = localStorage.getItem('token');
  const extendedConfig = { ...config };
  if (savedToken) extendedConfig.headers.Authorization = `Bearer ${savedToken}`;
  return extendedConfig;
})

export default jacsAxios;
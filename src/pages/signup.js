import React, { useState, useEffect } from 'react'
import { Form, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'

import { Navigate } from 'react-router-dom'
import jacsAxios from '@util';

export default function SignUp() {
	const [username, setUserName] = useState('')
	const [email, setEmail] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [password, setPassword] = useState('')
	const [confirmPassword, setConfirmPassword] = useState('')
	const [isActive, setIsActive] = useState(false)
	const [willRedirect, setWillRedirect] = useState(false)

	const clearFields = () => {
		setUserName('');
		setEmail('');
		setMobileNo('');
		setPassword('');
		setConfirmPassword('');
	}

	useEffect(() => {
		const fieldsNotEmpty = username && email && mobileNo && password && confirmPassword;
		const passwordConfirmedIdentical = password === confirmPassword;
		if (fieldsNotEmpty && passwordConfirmedIdentical) setIsActive(true)
		else setIsActive(false)
	}, [username, email, mobileNo, password, confirmPassword]);

	const registerUser = async (e) => {
		e.preventDefault();
		try {
			await jacsAxios.post('/users/register', {
				username: username,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
			Swal.fire({
				icon: "success",
				title: "Registration Successful!",
				confirmButtonColor: '#eb967a'
			})
			clearFields();
			setWillRedirect(true);
		} catch (error) {
			Swal.fire({
				icon: "error",
				title: "Registration Failed",
				confirmButtonColor: '#eb967a',
				text: error.message || 'Something went wrong.'
			})
		}
	}
	return (
		willRedirect
			? <Navigate to="/" />
			: <div>
				<div className="row justify-content-center">
					<div id="register-box" className="col-lg-5">
						<div className="sign-header"> <h2>SIGN UP</h2> </div>
						<Form className="form-box" onSubmit={e => registerUser(e)}>
							<Form.Group className="form-item">
								<Form.Label className="form-label">USER NAME</Form.Label>
								<Form.Control
									type="text"
									value={username}
									onChange={e => { setUserName(e.target.value) }}
									required />
							</Form.Group>

							<Form.Group className="form-item">
								<Form.Label className="form-label">EMAIL</Form.Label>
								<Form.Control
									type="email"
									value={email}
									onChange={e => { setEmail(e.target.value) }}
									required />
							</Form.Group>

							<Form.Group className="form-item">
								<Form.Label className="form-label">MOBILE NUMBER</Form.Label>
								<Form.Control
									type="text"
									value={mobileNo}
									onChange={e => { setMobileNo(e.target.value) }}
									required />
							</Form.Group>

							<Form.Group className="form-item">
								<Form.Label className="form-label">PASSWORD</Form.Label>
								<Form.Control
									type="password"
									value={password}
									onChange={e => { setPassword(e.target.value) }}
									required />
							</Form.Group>

							<Form.Group className="form-item">
								<Form.Label className="form-label">CONFIRM PASSWORD</Form.Label>
								<Form.Control
									type="password"
									value={confirmPassword}
									onChange={e => { setConfirmPassword(e.target.value) }}
									required />
							</Form.Group>

							<div className="sign-button">
								<Button variant="custom" type="submit" disabled={!isActive}>
									Sign Up
								</Button>
							</div>
						</Form>
						<div className="signin-to-reg">
							Already a member? <a href="/signin">Sign In Here</a>
						</div>
					</div>
				</div>
			</div>
	)
}
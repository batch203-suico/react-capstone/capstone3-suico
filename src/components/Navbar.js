import React, { useContext, useEffect, useState } from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { Container, NavDropdown } from 'react-bootstrap'
import { NavLink, Link, useNavigate } from 'react-router-dom'
import userContext from '../userContext';

export default function NavBar() {

	const { user, unsetUser, setUser } = useContext(userContext)
	const [willRedirect, setWillRedirect] = useState(false)
	const navigate = useNavigate();

	function logout() {
		unsetUser();
		setUser({
			email: null,
			isAdmin: null
		});
		localStorage.removeItem('token');
		setWillRedirect(true);
		navigate('/signin');
	}

	const adminNavLinks = <>
		<Nav.Link className="navbar" as={NavLink} to="/products">Products</Nav.Link>
		<Nav.Link className="navbar" as={NavLink} to="/addproduct">Add Product</Nav.Link>
		<Nav.Link className="navbar" as={NavLink} to="/orders">Orders</Nav.Link>
	</>

	const customerNavLinks = <>
		<Nav.Link className="navbar" as={NavLink} to="/products">Products</Nav.Link>
	</>

	const hasNoEmailAccountDropdown = <>
		<NavDropdown.Item className="nav-dropdown-item" as={NavLink} to="/signup">Sign Up</NavDropdown.Item>
		<NavDropdown.Item className="nav-dropdown-item" as={NavLink} to="/signin">Sign in</NavDropdown.Item>
	</>

	const emailAccountDropdown = <>
		<NavDropdown.Item className="nav-dropdown-item" as={NavLink} to="/orders">Orders</NavDropdown.Item>
		<NavDropdown.Divider />
		<NavDropdown.Item className="nav-dropdown-item" as={NavLink} to="/userprofile">Profile</NavDropdown.Item>
		<NavDropdown.Divider />
		<NavDropdown.Item className="nav-dropdown-item" onClick={logout}>Logout</NavDropdown.Item>
	</>
	return (
		<>
			<Navbar className="navbar" expand="sm" sticky="top">
				<Container className="navbar">
					<Navbar.Brand id="brand" as={Link} to="/">Jacs-EXPRESS</Navbar.Brand>
					<Navbar.Toggle aria-controls="basic-navbar-nav" />
					<Navbar.Collapse className="navbar" id="basic-navbar-nav">
						<Nav className="ml-auto">
							{user.isAdmin ? adminNavLinks : customerNavLinks}
						</Nav>
						<NavDropdown title="Account" id="nav-dropdown" className="navbar mx-0">
							{user.isAdmin ? <NavDropdown.Item className="nav-dropdown-item" onClick={logout}>Logout</NavDropdown.Item>
								: user.email ? emailAccountDropdown : hasNoEmailAccountDropdown
							}
						</NavDropdown>
					</Navbar.Collapse>
				</Container>
			</Navbar>
		</>
	)
}
import React, { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import { Navigate, useRouteError } from 'react-router-dom'
import Swal from 'sweetalert2'
import jacsAxios from '@util';

import userContext from '../userContext'

export default function SignIn() {

	const { user, setUser } = useContext(userContext)

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(false)
	const [willRedirect, setWillRedirect] = useState(false)

	const signInUser = async (e) => {
		e.preventDefault();
		try {
			const response = await jacsAxios.post('/users/login', { email, password });
			const { data: { accessToken, userDetails } } = response;

			localStorage.setItem('token', accessToken)
			localStorage.setItem('email', userDetails.email)
			localStorage.setItem('isAdmin', userDetails.isAdmin)
			localStorage.setItem('username', userDetails.username)

			setUser({ email: userDetails.email, isAdmin: userDetails.isAdmin })
			setWillRedirect(true)
			Swal.fire({
				icon: "success",
				title: "Login Successful!",
				text: response?.data?.message || '',
			})
		} catch (error) {
			Swal.fire({
				icon: "error",
				title: "Login Failed!",
				text: error.response?.data?.message || '',
			})
		}	
		setEmail('')
		setPassword('')
	}

	return (
		user.email || willRedirect
			? <Navigate to='/' />
			:
			<div id="sign-section">
				<div className="row justify-content-center">
					<div id="signin-box" className="col-lg-4">
						<div className="sign-header"> <h2>SIGN IN</h2> </div>
						<Form onSubmit={e => signInUser(e)} className="form-box">
							<Form.Group className="form-item">
								<Form.Label>EMAIL</Form.Label>
								<Form.Control
									type="email"
									value={email}
									onChange={e => { setEmail(e.target.value) }}
									required />
							</Form.Group>
							<Form.Group className="form-item">
								<Form.Label>PASSWORD</Form.Label>
								<Form.Control
									type="password"
									value={password}
									onChange={e => { setPassword(e.target.value) }}
									required />
							</Form.Group>
							<div className="sign-button">
								<Button variant="custom" type="submit"> Sign In </Button>
							</div>
						</Form>
						<div className="signin-to-reg">
							Not yet a member?
							<a href="/signup">Sign Up Here</a>
						</div>
					</div>
				</div>
			</div>
	)
}




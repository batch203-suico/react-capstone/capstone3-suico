import React from 'react'
import Carousel from 'react-bootstrap/Carousel'
import image1 from './images/Carousel-1.jpg'
import image2 from './images/Carousel-2.jpg'


export default function Home () {



	return (

		<Carousel className="carousel" variant="none">
		  <Carousel.Item className="carousel-item">
		    <img
		      className="d-block w-100"
		      src={image1}
		      alt="First slide"
		    />
		    <Carousel.Caption>
		      <h5>Shop Now</h5>
		      <p>We find ways</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item className="carousel-item">
		    <img
		      className="d-block w-100"
		      src={image2}
		      alt="Second slide"
		    />
		    <Carousel.Caption>
		      <h5>Get Discount</h5>
		      <p>See our latest products</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		</Carousel>

		)
}
import { useState } from 'react';
import { Button } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2';
import jacsAxios from '@util';

export default function ProductLineItem({ product }) {
  const [storedData, setStoredData] = useState({});
  const [isActive, setIsActive] = useState(product.isActive);
  const savedToken = localStorage.getItem('token');
  const [willRedirect, setWillRedirect] = useState(false)


  const archive = async (productId) => {
    await jacsAxios.put(`/products/${productId}`, { isActive: false })
    setIsActive(false);
    Swal.fire({
      icon: "success",
      confirmButtonColor: '#eb967a',
      title: "Product Successfully Archived!"
    })
  }

  async function activate(productId) {
    await jacsAxios.put(`/products/${productId}`, { isActive: true })
    setIsActive(true);
    Swal.fire({
      icon: "success",
      confirmButtonColor: '#eb967a',
      title: "Product Successfully Activated!"
    })
  }

  async function deleteProduct(productId) {
    await jacsAxios.delete(`/products/${productId}`)
    Swal.fire({
      icon: "success",
      confirmButtonColor: '#eb967a',
      title: "Product Successfully Deleted!"
    })
  }

  async function editDetails(productId) {
    const { data } = await jacsAxios.get(`/products/${productId}`);
    let dataStored = data.searchResult
    setStoredData(dataStored);
    setWillRedirect(true);
  }

  return (
    <>
      {willRedirect && (<Navigate to={{
        pathname: `/${product._id}/editproductdetails`,
        state: storedData
      }} />)}
      <tr key={product._id}>
        <td>{product._id}</td>
        <td>{product.name}</td>
        <td>{product.price}</td>
        <td>{product.stock}</td>
        <td>{product.createdAt}</td>
        <td>{product.updatedAt}</td>
        <td className={product.isActive ? "text-success" : "text-muted"}>{isActive ? "Active" : "Inactive"}</td>
        <td>
          {isActive
            ? <Button className="archive-btn" onClick={() => archive(product._id)}>Archive</Button>
            : <Button className="activate-btn" onClick={() => activate(product._id)}>Activate</Button>
          }
          <Button className="edit-btn" onClick={() => editDetails(product._id)}>Edit</Button>
          <Button className="delete-btn" onClick={() => deleteProduct(product._id)}>Delete</Button>
        </td>
      </tr>
    </>
  )

}
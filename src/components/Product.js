import React, { useState, useContext } from 'react'
import { Card } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
import nextArrow from '../pages/images/next-arrow.png'
import userContext from '../userContext'
import jacsAxios from '@util';

export default function Product({ productProp }) {
	const { user } = useContext(userContext)
	const [willRedirect, setWillRedirect] = useState(false)
	const [storedData, setStoredData] = useState({})

	async function viewProduct(productId) {
		const data = await jacsAxios.get(`/products/${productId}`)
		const retrievedProduct = data.searchResult;
		setStoredData(retrievedProduct);
		setWillRedirect(true);
	}

	const hasEmailRedirect = <a onClick={() => viewProduct(productProp._id)}>
		<img src={nextArrow} alt="go to product" align="right" />
	</a>

	const hasNoEmailRedirect = <a href="/signin">
		<img src={nextArrow} alt="go to login" align="right" />
	</a>
	return (
		<>
			{willRedirect && (<Navigate to={{
				pathname: `/${productProp._id}/productdetails`,
				state: storedData
			}} />)}

			<div className="col-lg-4" id="productColumn">
				<Card className="productCard">
					<Card.Img variant="top" src={productProp.productImage} />
					<Card.Body>
						<Card.Title>{productProp.name}</Card.Title>
						<Card.Text>{productProp.description}</Card.Text>
						<Card.Text>Product Owner: {productProp.owner}</Card.Text>
					</Card.Body>
					<div className="productPrice">
						<div className="row">
							<div className="col details align-middle">
								<div><p className="product-price align-middle">Price: ₱ {productProp.price.toFixed(2)}</p></div>
								<div><p className="product-price align-middle">Stock: {productProp.stock} pcs</p></div>
							</div>
							<div className="col-lg-3 details">
								<div className="next-arrow"> {user.email ? hasEmailRedirect : hasNoEmailRedirect} </div>
							</div>
						</div>
					</div>
				</Card>
			</div>
		</>

	)
}
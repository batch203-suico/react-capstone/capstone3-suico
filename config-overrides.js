const { alias } = require('react-app-rewire-alias')

module.exports = function override(config) {
  alias({
    '@util': 'src/util',
  })(config)

  return config
}
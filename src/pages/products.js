import React, { useState, useEffect, useContext } from 'react'
import { Table, Button } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import jacsAxios from '@util';

import Product from '../components/Product'
import userContext from '../userContext'
import Dropdown from 'react-bootstrap/Dropdown';
import ProductLineItem from '../components/ProductLineItem';

export default function Products() {

	const { user } = useContext(userContext)
	const [allProducts, setAllProducts] = useState([])
	const [activeProducts, setActiveProducts] = useState([])
	const [update, setUpdate] = useState(0)
	const [willRedirect, setWillRedirect] = useState(false)
	const [storedData, setStoredData] = useState({})
	const [showActiveProducts, setShowActiveProducts] = useState(false);
	const [showInactiveProducts, setShowInactiveProducts] = useState(false);

	useEffect(() => {
		const fetchProducts = async () => {
			let url = '/products/';
			if (showActiveProducts) url += '?activeOnly=true';
			if (showInactiveProducts) url += '?inactiveOnly=true';
			const response = await jacsAxios.get(url);
			const { data: retrievedProducts } = response;

			if (!retrievedProducts.length) return []; // Guard Clause

			setAllProducts(retrievedProducts)
			setActiveProducts(retrievedProducts.filter(({ isActive }) => isActive));
		}
		fetchProducts();
	}, [update, showActiveProducts, showInactiveProducts])


	const productComponents = activeProducts
		&& activeProducts.map((product) => <Product key={product._id} productProp={product} />);

	const productRows = allProducts && allProducts.map(product => <ProductLineItem key={product._id} product={product} />)

	const adminVisibleComponents = <>
		{willRedirect && (<Navigate to={{ pathname: "/editproductdetails", state: storedData }} />)}
		<div className="table-container">
			{
				!allProducts.length
					? <div className="cart-msg"> No existing product as of the moment. </div>
					: <>
						<Table className="products-list" striped bordered hover>
							<thead>
								<tr>
									<th>Product ID</th>
									<th>Name</th>
									<th>Price</th>
									<th>Stocks</th>
									<th>Created</th>
									<th>Updated</th>
									<th>
										<Dropdown>
											<Dropdown.Toggle variant="success" id="dropdown-basic"> Status </Dropdown.Toggle>
											<Dropdown.Menu>
												<Dropdown.Item ><Button className="edit-btn" onClick={() => setShowInactiveProducts(true)}>All</Button></Dropdown.Item>
												<Dropdown.Item onClick={setShowActiveProducts}>Active</Dropdown.Item>
												<Dropdown.Item onClick={setShowInactiveProducts}>Inactive</Dropdown.Item>
											</Dropdown.Menu>
										</Dropdown>
									</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody> {productRows} </tbody>
						</Table>
					</>
			}
		</div>
	</>

	const customerVisibleComponents = <>
		<div>
			<div className="row">
				<div className="col-lg-10 page-heading"> PRODUCTS </div>
			</div>
		</div>
		{productComponents}
	</>

	return user.isAdmin ? adminVisibleComponents : customerVisibleComponents
}
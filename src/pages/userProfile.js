import React, { useEffect, useState } from 'react'
import { Table, Card } from 'react-bootstrap'
import jacsAxios from '@util';
import moment from 'moment';

export default function UserProfile() {

	const [userOrders, setUserOrders] = useState([])

	useEffect(() => {
		const getUserDetails = async () => {
			const response = await jacsAxios.get('/orders');
			if (response.data?.orders) setUserOrders(response.data?.orders ?? [])
		}
		getUserDetails();
	}, [])

	const formatDate = (date) => {
		const FORMAT = 'YYYY-MM-DD HH:mm:ss';
		return moment(date).format(FORMAT);
	}

	let userOrdersList = userOrders && userOrders.map(userOrder => {
		return (
			<tr key={userOrder._id}>
				<div>
					{
						userOrder.status ? <div className="order-fulfilled">Order Complete</div>
							: <div className="order-pending">To Receive</div>
					}
					<div className="row">
						<div className="col"> Reference #: {userOrder._id} </div>
					</div>
					<div className="row">
						<div className="col"> Date of Purchase: {userOrder.purchasedOn ? formatDate(userOrder.purchasedOn) : ''}</div>
					</div>
					<div className="row">
						<div className="col"> Total Payment: ₱ {userOrder.totalAmount?.toFixed(2) ?? ''} </div>
					</div>
				</div>
			</tr>
		)
	})

	const noOrdersComponent = <div className="cart-msg"> You have no orders. </div>
	return (
		<div>
			<div className="row justify-content-center">
				<div className="col-lg-7">
					<Card className="cart">
						<div>
							<div className="row justify-content-center">
								<div className="col-lg-10 purchases-heading"> PURCHASES </div>
							</div>
						</div>
						<Table className="my-orders-list" striped bordered hover>
							{!userOrders.length ? noOrdersComponent : <tbody> {userOrdersList} </tbody>
							}
						</Table>
					</Card>
				</div>
			</div>
		</div>

	)
}
import React, { useState } from 'react';
import { BrowserRouter as Router, Routes, Route, Link, useNavigate } from 'react-router-dom'


import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap'
import './App.css'

import { UserProvider } from './userContext'

import NavBar from './components/Navbar'
import Home from './pages/home'
import SignUp from './pages/signup'
import SignIn from './pages/signin'
import Products from './pages/products'
import ViewProductDetails from './pages/productDetails'
import EditProductDetails from './pages/editProductDetails'
import AddProduct from './pages/addProducts'
import Cart from './pages/cart'
import CheckOut from './pages/checkOut'
import Orders from './pages/orders'
import UserProfile from './pages/userProfile'


function App() {

  const [cart, setCart] = useState([])

  const [user, setUser] = useState({
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === "true"
  })

  function unsetUser() {
    localStorage.clear()
  }


  function addProductToCart(product) {
    let index = cart.findIndex(item => item.productId === product.productId)
    if (index === -1) {
      cart.push(product)
    } else {
      cart[index].quantity += product.quantity
    }
    setCart(cart)
  }

  function emptyCart() {
    setCart([])
  }


  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <NavBar />
          <Container>
            <Routes>
              <Route exact path="/" element={<Home />} />
              <Route exact path="/signup" element={<SignUp />} />
              <Route exact path="/signin" element={<SignIn />} />
              <Route exact path="/orders" element={<Orders />} />
              <Route exact path="/userprofile" element={<UserProfile />} />
              <Route exact path="/:productId/productdetails" element={<ViewProductDetails />} />
              {/* <Route exact path="/:productId/productdetails" render={() => <ViewProductDetails addProductToCart={addProductToCart} />}
              /> */}
              <Route exact path="/cart" render={() => <Cart cart={cart} emptyCart={emptyCart} />}
              />
              <Route exact path="/checkout" element={<CheckOut />} />
              <Route exact path="/:productId/editproductdetails" element={<EditProductDetails />} />
              <Route exact path="/addproduct" element={<AddProduct />} />
              <Route className="row" exact path="/products" element={<Products />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>

    </>

  );

}

export default App;

import React, { useState, useEffect, useContext } from 'react'
import { Navigate } from 'react-router-dom'
import { Form, Button, InputGroup } from 'react-bootstrap'
import Swal from 'sweetalert2'
import jacsAxios from '@util';
import userContext from '../userContext'

export default function AddProduct() {
	const { user } = useContext(userContext)
	const initialProductState = {
		name: '',
		description: '',
		owner: '',
		stock: 0,
		price: 0,
		isActive: undefined,
	}
	const [product, setProduct] = useState(initialProductState)
	const [willRedirect, setWillRedirect] = useState(false)

	// useEffect(() => {
	// 	if (!product.name && !product.description
	// 		&& (!product.stock && product.stock > 0)
	// 		&& (!product.price && product.price > 0)) {
	// 		setProduct({ ...product, isActive: true })
	// 	} else setProduct({ ...product, isActive: false })
	// }, [product])


	async function addProduct(e) {
		try {
			await jacsAxios.post('/products/', { ...product })
			Swal.fire({
				icon: "success",
				title: "Product Added Successfully!",
				text: "Product has been created"
			})
		} catch (error) {
			Swal.fire({
				icon: "error",
				title: error.statusText,
				text: error.message,
			})
		}
		setWillRedirect(true);
	}

	return (
		!user.isAdmin || willRedirect
			?
			<Navigate to="/products" />
			:
			<div className="create-product-section">
				<div className="row justify-content-center">
					<div id="create-product-box" className="col-lg-5">
						<div className="sign-header">
							<h2>ADD PRODUCT</h2>
						</div>
						<Form onSubmit={e => addProduct(e)} className="form-box">
							<Form.Group className="form-item">
								<Form.Label>NAME</Form.Label>
								<Form.Control
									type="text"
									value={product.name}
									onChange={e => setProduct({ ...product, name: e.target.value })}
									required />
							</Form.Group>

							<Form.Group className="form-item">
								<Form.Label>DESCRIPTION</Form.Label>
								<Form.Control
									as="textarea"
									value={product.description}
									onChange={e => setProduct({ ...product, description: e.target.value })}
									required />
							</Form.Group>

							<Form.Group className="form-item">
								<Form.Label>STOCKS</Form.Label>
								<InputGroup>
									<Form.Control
										type="number"
										value={product.stock}
										onChange={e => setProduct({ ...product, stock: e.target.value })} />
									<InputGroup.Text>pcs</InputGroup.Text>
								</InputGroup>
							</Form.Group>

							<Form.Group className="form-item">
								<Form.Label>OWNER</Form.Label>
								<Form.Control
									type="text"
									value={product.owner}
									onChange={e => setProduct({ ...product, owner: e.target.value })}
									required />
							</Form.Group>

							<Form.Group className="form-item">
								<Form.Label>PRICE</Form.Label>
								<InputGroup>
									<InputGroup.Text>Php</InputGroup.Text>
									<Form.Control
										type="number"
										value={product.price}
										onChange={e => setProduct({ ...product, price: e.target.value })} />
								</InputGroup>
							</Form.Group>

							<div className="sign-button">
								<Button variant="custom" type="submit">Add</Button>
							</div>
						</Form>
					</div>
				</div>
			</div>



	)
}

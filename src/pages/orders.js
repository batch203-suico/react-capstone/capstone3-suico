import React, { useContext, useEffect, useState } from 'react'
import { Navigate } from 'react-router-dom'
import { Button, Table, Modal } from 'react-bootstrap'
import userContext from '../userContext'
import jacsAxios from '@util';

export default function Orders() {
	const { user } = useContext(userContext)
	const [order, setOrder] = useState({})
	const [modalShow, setModalShow] = useState(false)
	const [allOrders, setAllOrders] = useState([])
	const [update, setUpdate] = useState(0)

	useEffect(() => {
		const getAllOrders = async () => {
			const response = await jacsAxios.get('/orders');
			if (!response.data) return;
			setAllOrders(response.data.orders);
		}
		if (user.isAdmin) getAllOrders();
	}, []);

	function fulfillOrder(orderId) {
		fetch(`http://localhost:4000/orders/${orderId}`, {
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
			.then(res => res.json())
			.then(data => {
				setUpdate({})
			})
	}

	async function viewOrder(orderId) {
		const response = await jacsAxios.get(`/orders/${orderId}`);
		setOrder(response.data);
		setModalShow(true);
	}

	function MyVerticallyCenteredModal(props) {
		let orderedProducts = order.products?.map(product => {
			return (
				<div key={product.productId}>
					<div>
						<div className="row">
							<div className="col modal-view"> Product Reference ID: </div>
							<div className="col modal-view-right"> {product.productId} </div>
						</div>
						<div className="row">
							<div className="col modal-view"> Quantity: </div>
							<div className="col modal-view-right"> {product.quantity} pcs. </div>
						</div>
						<div className="row">
							<div className="col modal-view"> Price per unit: </div>
							<div className="col modal-view-right"> ₱{product.price} </div>
						</div>
					</div>
					<hr />
				</div>
			)
		})

		return (
			<Modal {...props} size="m" aria-labelledby="contained-modal-title-vcenter" centered>
				<Modal.Header closeButton>
					<Modal.Title className="modal-view" id="contained-modal-title-vcenter">
						Reference ID: {order._id}
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					{orderedProducts}
					<div>
						<div className="row"></div>
						<div className="row">
							<div className="col modal-view-left"> <strong>Total Payment:</strong> </div>
							<div className="col modal-view-right"> ₱ {order.totalAmount && order.totalAmount.toFixed(2)} </div>
						</div>
					</div>
				</Modal.Body>
			</Modal>
		);
	}

	let ordersList = allOrders && allOrders.map(order => {
		return (
			<tr key={order._id}>
				<td>{order._id}</td>
				<td>{order.user.email}</td>
				<td>{order.purchasedOn}</td>
				<td className={order.status ? "text-success" : "text-muted"}>{order.status ? "Fulfilled" : "Pending"}</td>
				<td>
					{!order.status &&
						<Button className="activate-btn" onClick={() => fulfillOrder(order._id)}>
							Fulfill
						</Button>
					}
					<Button className="edit-btn" onClick={() => { viewOrder(order._id); }}>
						View
					</Button>
				</td>
			</tr>
		)
	})

	const adminOrdersComponent = <div className="table-container">
		<Table className="products-list" striped bordered hover>
			<thead>
				<tr>
					<th>Order ID</th>
					<th>User</th>
					<th>Purchase Date</th>
					<th>Status</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody> {ordersList} </tbody>
		</Table>
		<MyVerticallyCenteredModal show={modalShow} onHide={() => setModalShow(false)} />
	</div>

	return user.isAdmin ? adminOrdersComponent : <Navigate to="/userprofile" />
}
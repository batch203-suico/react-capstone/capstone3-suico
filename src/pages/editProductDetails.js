import React, { useState, useEffect, useContext } from 'react'
import { useLocation, Navigate } from "react-router-dom";
import { Form, Button, InputGroup } from 'react-bootstrap'
import Swal from 'sweetalert2'
import jacsAxios from '@util';

import userContext from '../userContext'

export default function EditProductDetails() {
	const initialProductState = {
		name: '',
		description: '',
		owner: '',
		stock: 0,
		price: 0,
		isActive: false,
	}
	const [product, setProduct] = useState(initialProductState);
	const { user } = useContext(userContext)
	const [willRedirect, setWillRedirect] = useState(false)

	let location = useLocation();
	const split = location.pathname.split('/');
	const productId = split[1];

	useEffect(() => {
		const fetchProducts = async () => {
			const response = await jacsAxios.get(`/products/${productId}`);
			const retrievedProduct = response.data;
			setProduct({
				name: retrievedProduct.name,
				description: retrievedProduct.description,
				stock: retrievedProduct.stock,
				price: retrievedProduct.price,
				owner: retrievedProduct.owner,
				isActive: retrievedProduct.isActive,
			})
		}
		fetchProducts();
	}, [productId]);

	// useEffect(() => {
	// 	if (product.price > 0 && product.name && !product.description) {
	// 		setProduct(true);
	// 		setProduct({ ...product, isActive: true });
	// 	} else setProduct({ ...product, isActive: false })
	// }, [product])

	async function editProduct(e) {
		e.preventDefault();
		const updatedProduct = { ...product };
		delete updatedProduct.isActive;
		try {
			await jacsAxios.put(`/products/${productId}`, { ...updatedProduct });
			Swal.fire({
				icon: "success",
				confirmButtonColor: '#eb967a',
				title: "Product Updated Successfully!"
			})
			setWillRedirect(true);
		} catch (error) {
			Swal.fire({
				icon: "error",
				title: error.statusText || 'Something went wrong.',
				confirmButtonColor: '#eb967a',
				text: error.message,
			})
		}
	}
	return (
		user.isAdmin === false || willRedirect
			?
			<Navigate to="/products" />
			:
			<div className="create-product-section">
				<div className="row justify-content-center">
					<div id="create-product-box" className="col-lg-5">
						<div className="sign-header">
							<h2>EDIT PRODUCT DETAILS</h2>
						</div>
						<Form onSubmit={e => editProduct(e)} className="edit-product-form">
							<Form.Group className="form-item">
								<Form.Label>NAME:</Form.Label>
								<Form.Control
									type="text"
									value={product.name}
									onChange={e => setProduct({ ...product, name: e.target.value })} />
							</Form.Group>

							<Form.Group className="form-item">
								<Form.Label>DESCRIPTION:</Form.Label>
								<Form.Control
									as="textarea"
									value={product.description}
									onChange={e => setProduct({ ...product, description: e.target.value })} />
							</Form.Group>

							<Form.Group className="form-item">
								<Form.Label>STOCKS:</Form.Label>
								<InputGroup>
									<Form.Control
										type="number"
										value={product.stock}
										onChange={e => setProduct({ ...product, stock: e.target.value })} />
									<InputGroup.Text>pcs</InputGroup.Text>
								</InputGroup>
							</Form.Group>

							<Form.Group className="form-item">
								<Form.Label>OWNER:</Form.Label>
								<InputGroup>
									<Form.Control
										type="text"
										value={product.owner}
										onChange={e => setProduct({ ...product, owner: e.target.value })} />
								</InputGroup>
							</Form.Group>

							<Form.Group className="form-item">
								<Form.Label>PRICE:</Form.Label>
								<InputGroup>
									<InputGroup.Text>Php</InputGroup.Text>
									<Form.Control
										type="number"
										value={product.price}
										onChange={e => setProduct({ ...product, price: e.target.value })} />
								</InputGroup>
							</Form.Group>

							<div className="sign-button">
								<Button
									variant="custom"
									// disabled={!product.isActive}
									type="submit">
									Save
								</Button>
							</div>
						</Form>
					</div>
				</div>
			</div>

	)
}
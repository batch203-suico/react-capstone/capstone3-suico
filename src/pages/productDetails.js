import React, { useState, useContext, useEffect } from 'react'
import { useLocation, Navigate } from "react-router-dom";
import { Card, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'
import jacsAxios from '@util';

import cart from './images/cart.png'
import userContext from '../userContext';

export default function ViewProductDetails({ addProductToCart }) {
	const initialProductState = {
		id: '',
		name: '',
		description: '',
		owner: '',
		stock: 0,
		price: 0,
		isActive: false,
		orderedQuantity: 0,
	}
	let location = useLocation();
	const { user } = useContext(userContext)
	const split = location.pathname.split('/');
	const productId = split[1];

	const [product, setProduct] = useState(initialProductState);
	const [willRedirect, setWillRedirect] = useState(false)
	const [totalOrderAmount, setTotalOrderAmount] = useState(0);

	async function placeOrder() {
		const payload = {
			userId: user.id,
			products: [{ productId: product.id, quantity: product.orderedQuantity }],
			totalAmount: totalOrderAmount,
		}
		try {
			await jacsAxios.post('/orders/checkout', payload);
			Swal.fire({
				icon: "success",
				title: "Order Placed!",
				confirmButtonColor: '#eb967a',
				text: `Thank you for shopping with us, ${user.username}!`
			})
			setWillRedirect(true)
		} catch (error) {
			Swal.fire({
				icon: "error",
				title: error.statusText,
				text: error.message,
			})
		}
	}

	useEffect(() => {
		const fetchProducts = async () => {
			const response = await jacsAxios.get(`/products/${productId}`);
			const retrievedProduct = response.data;
			setProduct({
				...product,
				id: retrievedProduct._id,
				name: retrievedProduct.name,
				description: retrievedProduct.description,
				owner: retrievedProduct.owner,
				stock: retrievedProduct.stock,
				price: retrievedProduct.price,
				isActive: retrievedProduct.isActive,
			})
		}
		fetchProducts();
	}, [productId]);


	function increment() {
		const updateDraft = {
			...product,
			stock: product.stock - 1,
			orderedQuantity: product.orderedQuantity + 1,
		}
		setTotalOrderAmount(updateDraft.orderedQuantity * product.price);
		console.table(updateDraft)
		setProduct(updateDraft)
	}

	function decrement() {
		setProduct({ ...product, stock: product.stock + 1, orderedQuantity: product.orderedQuantity - 1 })
		setTotalOrderAmount(product.orderedQuantity * product.price);
	}

	return (
		willRedirect
			?
			<Navigate to='/userprofile' />
			:
			<div>
				<div className="row justify-content-center">
					<div id="viewProduct" className="col-lg-6">
						<Card className="single-product-card">
							<Card.Body>
								<Card.Title>{product.name}</Card.Title>
								<Card.Text className="product-description">
									{product.description}
								</Card.Text>
							</Card.Body>
							<div className="productPrice">
								<div className="row">
									<div className="col product-price-stocks">
										<p>Price: ₱ {product.price.toFixed(2)}</p>
										<p>Stocks: {product.stock} pcs.</p>
									</div>
									<div>
										<h3>Total Amount: {totalOrderAmount.toFixed(2)}</h3>
									</div>
								</div>
								<div className="row" id="quantity-box">
									<div className="col">
										<Button
											disabled={!product.quantity}
											onClick={decrement}
											className="quantity-btn">-
										</Button>
										<span className="quantity-num" type="number ">{product.orderedQuantity}</span>
										<Button
											disabled={!product.stock}
											onClick={increment}
											className="quantity-btn">+
										</Button>
									</div>
									<div className="col-auto" id="cartbtn">
										<Button
											disabled={product.orderedQuantity <= 0}
											onClick={() => placeOrder()}
											className="add-to-cart-btn"><img src={cart} />  Place Order
										</Button>
									</div>
								</div>
							</div>
						</Card>
					</div>
				</div>
			</div>
	)
}

